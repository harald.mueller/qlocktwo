﻿$normalColor = "darkgray"
$lightColor = "yellow"
$indent = 10
[string]$s1 = ("").padLeft($indent)
[string]$s2 = ("").padLeft($indent+2)

function getColor {
    [int]$light = $args[0]

    $color = $normalColor
    if ($light -ne 0) {
        $color = $lightColor
    }    
    return $color
}

function setTxt {
    [string]$text = $args[0]
	[int]$light = $args[1]

    $col = getColor($light)
    Write-Host $text -foregroundColor $col -nonewline
} 

function zeigeUhr {

    $d = Get-Date
    $date = ($d.toString().split(" "))[0]
    $zeit = ($d.toString().split(" "))[1]
    $hms = $zeit.split(":")
    $h = $hms[0]; [int]$std = $h % 12
    $m = $hms[1]; [int]$min = $m
    $s = $hms[2]; [int]$sek = $s
    #"zeit=$zeit  $h $m $s   $std $min $sek"
    
    $min01   = $FALSE
    $min02   = $FALSE
    $min03   = $FALSE
    $min04   = $FALSE
    $min05   = $FALSE
    $min10   = $FALSE
    $min15   = $FALSE
    $min20   = $FALSE
    $halb    = $FALSE
    $vor     = $FALSE
    $nach    = $FALSE
    $std01   = $FALSE
    $std02   = $FALSE
    $std03   = $FALSE
    $std04   = $FALSE
    $std05   = $FALSE
    $std06   = $FALSE
    $std07   = $FALSE
    $std08   = $FALSE
    $std09   = $FALSE
    $std10   = $FALSE
    $std11   = $FALSE
    $std12   = $FALSE

    $punkt1  = " "
    $punkt2  = " "
    $punkt3  = " "
    $punkt4  = " "

    $mmod = $min % 5
    if ($mmod -ge 1) {$min01 = $TRUE; $punkt1 = "*"}
    if ($mmod -ge 2) {$min02 = $TRUE; $punkt2 = "*"}
    if ($mmod -ge 3) {$min03 = $TRUE; $punkt3 = "*"}
    if ($mmod -ge 4) {$min04 = $TRUE; $punkt4 = "*"}

    if ($min -ge 5  -AND $min -lt 10) {$min05 = $TRUE
                                       $nach  = $TRUE}
    if ($min -ge 10 -AND $min -lt 15) {$min10 = $TRUE
                                       $nach  = $TRUE}
    if ($min -ge 15 -AND $min -lt 20) {$min15 = $TRUE
                                       $nach  = $TRUE}
    if ($min -ge 20 -AND $min -lt 25) {$min20 = $TRUE
                                       $nach  = $TRUE}
    if ($min -gt 25 -AND $min -le 30) {$min05 = $TRUE
                                       $vor   = $TRUE
                                       $halb  = $TRUE}
    if ($min -gt 30 -AND $min -le 35) {$halb  = $TRUE}
    if ($min -gt 35 -AND $min -le 40) {$min05 = $TRUE
                                       $nach  = $TRUE
                                       $halb  = $TRUE}
    if ($min -ge 40 -AND $min -lt 45) {$min20 = $TRUE
                                       $vor   = $TRUE}
    if ($min -ge 45 -AND $min -lt 50) {$min15 = $TRUE
                                       $vor   = $TRUE}
    if ($min -ge 50 -AND $min -lt 55) {$min10 = $TRUE
                                       $vor   = $TRUE}
    if ($min -gt 55) {$min05 = $TRUE
                      $vor   = $TRUE}

    if ($min -gt 25) {$std++}
    if ($std -gt  1) {$uhr   = $TRUE}
  
    if ($std -eq  1) {$std01 = $TRUE}
    if ($std -eq  2) {$std02 = $TRUE}
    if ($std -eq  3) {$std03 = $TRUE}
    if ($std -eq  4) {$std04 = $TRUE}
    if ($std -eq  5) {$std05 = $TRUE}
    if ($std -eq  6) {$std06 = $TRUE}
    if ($std -eq  7) {$std07 = $TRUE}
    if ($std -eq  8) {$std08 = $TRUE}
    if ($std -eq  9) {$std09 = $TRUE}
    if ($std -eq 10) {$std10 = $TRUE}
    if ($std -eq 11) {$std11 = $TRUE}
    if ($std -eq 12) {$std12 = $TRUE}

    ""
    setTxt $s2"$date   $zeit"  $FALSE
    "`n"
    setTxt ($s1+$punkt3)       $min03
    setTxt $punkt2.padLeft(24) $min02
    ""
    setTxt $s2"E S "           $TRUE 
    setTxt "K "                $FALSE 
    setTxt "I S T "            $TRUE  
    setTxt "A "                $FALSE 
    setTxt "F Ü N F"           $min05
    ""
    setTxt $s2"Z E H N "       $min10
    setTxt "Z W A N Z I G "    $min20
    ""
    setTxt $s2"D R E I "       $std03
    setTxt "V I E R T E L "    $min15
    ""
    setTxt $s2"V O R "         $vor
    setTxt "F U N K "          $FALSE
    setTxt "N A C H "          $nach
    ""
    setTxt $s2"T "             $FALSE
    setTxt "H A L B "          $halb
    setTxt "L A "              $FALSE
    setTxt "F Ü N F"           $std05
    ""
    setTxt $s2"E I N S "       $std01
    setTxt "X A M "            $FALSE
    setTxt "Z W E I"           $std02
    ""
    setTxt $s2"D R E I "       $std03
    setTxt "E L F "            $std11
    setTxt "V I E R"           $std04
    ""
    setTxt $s2"S E C H S "     $std06
    setTxt "U L "              $FALSE
    setTxt "A C H T"           $std08
    ""
    setTxt $s2"S I E B E N "   $std07
    setTxt "Z W Ö L F"         $std12
    ""
    setTxt $s2"Z E H N "       $std10
    setTxt "N E U N "          $std09
    setTxt "U H R"             $uhr
    ""
    setTxt ($s1+$punkt4)       $min04
    setTxt $punkt1.padLeft(24) $min01
    "`n`n"
}

$z = 9
do {cls
    write-host "$s1$z "
    zeigeUhr
    $z-- 
    sleep 5
} until ($z -lt 0)
