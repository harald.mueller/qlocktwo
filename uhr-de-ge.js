var $textFarbe = "#ff0";//"yellow";
$textFarbe = "lightgreen";
$textFarbe = "red";
$textFarbe = "yellow";

var $colSek5 = "#0f0";
var $colSek = "#000";
var $colBG = "#333";
var $colBG2 = "#444";

/* Woerter in Funktionen */
function esist() 		{	setHighlight([r1d1,r1d2,r1d4,r1d5,r1d6]);	}
function uhr() 			{	setHighlight([r10d8,r10d9,r10d10]);	}
function vor() 			{	setHighlight([r3d9, r3d10, r3d11]);		}
function nach() 		{	setHighlight([r4d2,r4d3,r4d4,r4d5]);	}
/* minuten */
function fuenfMinuten()	{	setHighlight([r1d8,r1d9,r1d10,r1d11]);	}
function zehnMinuten()	{	setHighlight([r2d1,r2d2,r2d3,r2d4]);	}
function viertel() 		{	setHighlight([r3d1,r3d2,r3d3,r3d4,r3d5,r3d6,r3d7]);		}
function zwanzigMinuten(){	setHighlight([r2d5,r2d6,r2d7,r2d8,r2d9,r2d10,r2d11]);	}
function halb() 		{	setHighlight([r4d7,r4d8,r4d9,r4d10]);	}
/* stunden */
function ein() 			{	setHighlight([r5d1,r5d2,r5d3]);			}
function eins() 		{	setHighlight([r5d1,r5d2,r5d3,r5d4]);	}
function zwei() 		{	setHighlight([r5d8,r5d9,r5d10,r5d11]);	}
function drei() 		{	setHighlight([r6d1,r6d2,r6d3,r6d4]);	}
function vier() 		{	setHighlight([r6d8,r6d9,r6d10,r6d11]);	}
function fuenf() 		{	setHighlight([r7d1,r7d2,r7d3,r7d4]);	}
function sechs() 		{	setHighlight([r7d7,r7d8,r7d9,r7d10,r7d11]);		}
function sieben() 		{	setHighlight([r8d1,r8d2,r8d3,r8d4,r8d5,r8d6]);	}
function acht() 		{	setHighlight([r8d8,r8d9,r8d10,r8d11]);	}
function neun() 		{	setHighlight([r9d1,r9d2,r9d3,r9d4]);	}
function zehn() 		{	setHighlight([r9d5,r9d6,r9d7,r9d8]);	}
function elf() 			{	setHighlight([r9d9,r9d10,r9d11]);		}
function zwoelf() 		{	setHighlight([r10d1,r10d2,r10d3,r10d4,r10d5]);	}


/* Anzeige der Zeit */
function jsSekundenAnzeige(s) {
	var c = s % 5;
    var t = document.getElementsByTagName("table"), i;    
    for (i = 0; i < t.length; i++) {
    	if (c == 1) { s1d0.style.backgroundColor = $colSek;} else {s1d0.style.backgroundColor = $colBG;}
    	if (c == 2) { s1d1.style.backgroundColor = $colSek;} else {s1d1.style.backgroundColor = $colBG;}
       	if (c == 3) { s1d2.style.backgroundColor = $colSek;} else {s1d2.style.backgroundColor = $colBG;}
    	if (c == 4) { s1d3.style.backgroundColor = $colSek;} else {s1d3.style.backgroundColor = $colBG;}
    	if (c == 0) { s1d4.style.backgroundColor = $colSek;} else {s1d4.style.backgroundColor = $colBG;}
    	
    	if (s < 5) {
    		s5d0.style.backgroundColor = $colBG;	
    		s5d1.style.backgroundColor = $colBG;	
    		s5d2.style.backgroundColor = $colBG;	
    		s5d3.style.backgroundColor = $colBG;	
    		s5d4.style.backgroundColor = $colBG;	
    		s5d5.style.backgroundColor = $colBG;	
    		s5d6.style.backgroundColor = $colBG;	
    		s5d7.style.backgroundColor = $colBG;	
    		s5d8.style.backgroundColor = $colBG;	
    		s5d9.style.backgroundColor = $colBG;	
    		s5d10.style.backgroundColor = $colBG;	
    		s5d11.style.backgroundColor = $colBG;	
    	}	
    	
    	if (s >= 0 ) { s5d0.style.backgroundColor = $colSek5;	}
    	if (s >= 5 ) { s5d1.style.backgroundColor = $colSek5;	}
    	if (s >= 10) { s5d2.style.backgroundColor = $colSek5;	}
    	if (s >= 15) { s5d3.style.backgroundColor = $colSek5;	}
    	if (s >= 20) { s5d4.style.backgroundColor = $colSek5;	}
    	if (s >= 25) { s5d5.style.backgroundColor = $colSek5;	}
    	if (s >= 30) { s5d6.style.backgroundColor = $colSek5;	}
    	if (s >= 35) { s5d7.style.backgroundColor = $colSek5;	}
    	if (s >= 40) { s5d8.style.backgroundColor = $colSek5;	}
    	if (s >= 45) { s5d9.style.backgroundColor = $colSek5;	}
    	if (s >= 50) { s5d10.style.backgroundColor = $colSek5;	}
    	if (s >= 55) { s5d11.style.backgroundColor = $colSek5;	}
    }
}

function jsInit() {
    var html = document.getElementsByTagName("html"), i;   
    for (i = 0; i < html.length; i++) {
	    html[i].style.background = $colBG;
	    html[i].style.margin = "6px 14px 0 10px;";
    }
    var td = document.getElementsByTagName("td"), i;    
    for (i = 0; i < td.length; i++) {
        td[i].style.color = $colBG2;
        td[i].style.fontSize = "25pt";
        td[i].style.fontBold = "200";
        td[i].style.textShadow = "0 0 0 rgba(255,255,255,0.1)";
    }
}

function setHighlight(alleElemente) {
	alleElemente.forEach(function (einElement, i, thisArray) {
		einElement.style.color = $textFarbe;
		einElement.style.fontSize = "26pt";
		einElement.style.fontWeight = "bold";
		einElement.style.textShadow = "3px 3px 3px rgba(255,255,255,0.7)";
	});	
}

function showHour(h, m) {
  if (m > 24) { h++; } //wegen (vor) halb xx Uhr
  switch(h) {
  	case  0: zwoelf(); 	break;
    case  1: if ( m < 6 || m > 49) {
        		ein(); 	    		
    		 } else {
    			eins(); 	    		
    		 }			break;
    case  2: zwei(); 	break;
    case  3: drei(); 	break;
    case  4: vier(); 	break;
    case  5: fuenf(); 	break;
    case  6: sechs(); 	break;
    case  7: sieben(); 	break;
    case  8: acht(); 	break;
    case  9: neun(); 	break;
    case 10: zehn(); 	break;
    case 11: elf(); 	break;
  	case 12: zwoelf();  break;
  }
}

function showMin(m) {
	esist();
	if(m <   5)				{	uhr();	  							}
	if(m >=  5 &&  m < 10)	{ 	fuenfMinuten();		nach();			}
	if(m >= 10 &&  m < 15)	{	zehnMinuten();		nach(); 		}
	if(m >= 15 &&  m < 20)	{	viertel();			nach();			}
	if(m >= 20 &&  m < 25)	{	zwanzigMinuten();	nach();			}
	if(m >= 25 &&  m < 30)	{	fuenfMinuten();		vor(); 	halb();	}
	if(m >= 30 &&  m < 35)	{								halb();	uhr(); }
	if(m >= 35 &&  m < 40)	{	fuenfMinuten();		nach(); halb();	}
	if(m >= 40 &&  m < 45)	{	zwanzigMinuten();	vor();			}
	if(m >= 45 &&  m < 50)	{	viertel(); 			vor();			}
	if(m >= 50 &&  m < 55)	{	zehnMinuten();		vor();	uhr();	}
	if(m >= 55 &&  m < 60)	{	fuenfMinuten();		vor();	uhr();	}

	min5 = m % 5;
	if (min5 == 0) {	
		m1.style.color = $colBG; m1.style.textShadow = "1px 1px 1px rgba(0,0,0,0.9)";
		m2.style.color = $colBG; m2.style.textShadow = "1px 1px 1px rgba(0,0,0,0.9)";
		m3.style.color = $colBG; m3.style.textShadow = "1px 1px 1px rgba(0,0,0,0.9)";
		m4.style.color = $colBG; m4.style.textShadow = "1px 1px 1px rgba(0,0,0,0.9)";
	}
	if (min5 > 0) {		m1.style.color = $textFarbe; m1.style.textShadow = "2px 2px 2px rgba(255,255,255,0.8)";	}
	if (min5 > 1) {		m2.style.color = $textFarbe; m2.style.textShadow = "2px 2px 2px rgba(255,255,255,0.8)";	}
	if (min5 > 2) {		m3.style.color = $textFarbe; m3.style.textShadow = "2px 2px 2px rgba(255,255,255,0.8)";	}
	if (min5 > 3) {		m4.style.color = $textFarbe; m4.style.textShadow = "2px 2px 2px rgba(255,255,255,0.8)";	}
}


function main() {
	jsInit();
	var d = new Date();
	var h = d.getHours() % 12;
	var m = d.getMinutes();
	var s = d.getSeconds();
	if (m <=9) {m="0"+m;}
	if (s <=9) {s="0"+s;}	
	jsSekundenAnzeige(s);
	showHour(h, m);
	showMin(m);
	zeit.innerHTML = h+":"+m+":"+s;
}

intervall = setInterval(main, 500);

